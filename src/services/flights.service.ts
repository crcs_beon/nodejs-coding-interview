import { FlightsModel } from '../models/flights.model'
import { PersonsModel } from '../models/persons.model';

export class FlightsService {
    async getAll() {
        return await FlightsModel.find()
    }

    async onboard(code: string, email: string) {
        // validate flight existence -> readFlightOrFail -> flight
        // validate person existence
        // if email not in flight.passengers, insert into it and save
        // return flight, but removing other passengers.
        let isNewFlight = false;
        let [flight] = await FlightsModel.find({ code });
        if (!flight) {
            console.warn(`flight '${code}' not found`);
            // this is just for my local, flights were not inserted during db initialization
            flight = {
                code,
                origin: "EZE",
                destination: "LDN",
                passengers: [],
                status: "available"
            };
        }
        const [person] = await PersonsModel.find({ email });
        if (!person) {
            return Promise.reject(new Error(`person '${email}' not found`));
        }
        if (!flight.passengers.includes(email)) {
            flight.passengers.push(email);
        }
        try {
            flight.save();
        } catch (err) {
            flight = await FlightsModel.create(flight);
        }
        return { ...flight._doc, passengers: [email] };
    }
}

import { JsonController, Get, Put, Body, Param } from 'routing-controllers'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService()

@JsonController('/flights')
export default class FlightsController {
    @Get('', { transformResponse: false })
    getAll() {
        return {
            status: 200,
            data: flightsService.getAll(),
        }
    }

    // I'm using a PUT as it must be idempotent, onboarding the same passenger two times, must not have a different effect as doing it only one time
    @Put('/:code/onboard', { transformResponse: false })
    onboardPerson(@Param('code') code: string, @Body() body: { email: string }) {
        return flightsService.onboard(code, body.email)
    }
}
